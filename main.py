import settings
import discord
import requests
import random
import urllib.request
import json
from discord.ext import commands

errors = ["We've encountered a problem! Fucking shit devs right?",
          "I swear it was Al-Gore, or some other clone of him.",
          "I don't know who fucked up",
          "Really, something isn't working and it's probably Trump's fault",
          "Did you know entering data correctly can save a life?"]

def run():
    intents = discord.Intents.default()
    intents.message_content = True

    bot = commands.Bot(command_prefix="!", intents=intents)

    @bot.event
    async def on_ready():
        print(bot.user)
        print(bot.user.id)

    @bot.command()
    async def ping(ctx):
        await ctx.send("pong")

    @bot.command()
    async def love(ctx):
        await ctx.send("Emily Barton! <3 <3 <3 <3 <3 <3 <3 <3 <3 <3")

    @bot.command()
    async def favorite(ctx):
        await ctx.send("Garchomp!")

    @bot.command()
    async def party(ctx):
        try:
            for x in range(6):
                random_num = random.randint(0, 800)
                try:
                    print(f"attempting to get https://pokeapi.co/api/v2/pokemon/{random_num}")
                    response = requests.get(f"https://pokeapi.co/api/v2/pokemon/{random_num}")
                except:
                    print(f"failed to get https://pokeapi.co/api/v2/pokemon/{random_num}")
                json = response.json()
                image = json['sprites']["front_default"]
                try:
                    urllib.request.urlretrieve(image, "./pokemon.png")
                    print("Successfully pulled image.")
                except:
                    print("Unable to save file.")
                await ctx.send(file=discord.File("./pokemon.png"))
        except:
            random_int = random.randint(0, 4)
            await ctx.send(f"CRITICAL ERROR: {errors[random_int]} ...")


    @bot.command()
    async def stats(ctx, args):
        message = ""
        image = ""
        try:
            response = requests.get(f"https://pokeapi.co/api/v2/pokemon/{args}")
            json = response.json()
            image = json['sprites']["front_default"]
            for stat in json['stats']:
                message += f"{stat['stat']['name']}: {stat['base_stat']}\n"
            await ctx.send(image)
            await ctx.send(message)
        except:
            await ctx.send(f"Unable to locate pokemon '{args}'.")

    @bot.command()
    async def moves(ctx, args):
        message = ""
        image = ""
        moves = []
        try:
            response = requests.get(f"https://pokeapi.co/api/v2/pokemon/{args}")
            json = response.json()
            for move in json['moves']:
                name = move['move']['name']
                moves.append(name)
            image = json['sprites']["front_default"]
        except:
            await ctx.send(f"Unable to locate pokemon '{args}'.")
        for move in moves:
            message += f"{move}, "
        await ctx.send(image)
        await ctx.send(message)


    bot.run(settings.DISCORD_API_SECRET)


if __name__ == "__main__":
    run()